const express=require('express');
const bodyParsar=require('body-parser');

const milkrateRouter=require('./milkrate');


const app=express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods","GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParsar.json());
app.use(bodyParsar.urlencoded());

app.use(milkrateRouter);
app.use(express.static('images'));
app.listen(9000,'0.0.0.0',()=>{
    console.log('server runs on port no. 9000');
});

