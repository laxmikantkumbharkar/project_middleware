const express=require('express');
const db=require('./db');
const utils=require('./utils');
const multer = require('multer');
const notification = require('./notification');
const upload = multer({dest: 'images/'});
const router=express.Router();


// Product related services
//========================================================================================
router.get('/product',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail,Punit from Products`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/rate',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail,Punit from Products order by Prate`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/name',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products order by Pname`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/butter',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pcategory="butter" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/icecream',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pcategory="ice-cream" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/paneer',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pcategory="paneer" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/ghee',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pcategory="ghee" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/milk',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pcategory="milk" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/sort/Pcategory',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products order by Pcategory`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/search/:text',(request,response)=>{
    console.log('product get search method requested');
    const Pname = request.params.text;
    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where LOWER(Pname) like '%${Pname}%' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/:Pid',(request,response)=>{
    console.log('product get method requested');
    const Pid=request.params.Pid;
    const statement=`select Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pid='${Pid}' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result[0]));
    });
});

router.get('/butter',(request,response)=>{
    console.log('product category butter get method requested');
    const Pcategory=request.params.Pcategory;
    const statement=`select * from Products where Pcategory= "butter" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/milk',(request,response)=>{
    console.log('product category butter get method requested');
    const Pcategory=request.params.Pcategory;
    const statement=`select * from Products where Pcategory= "milk" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/ghee',(request,response)=>{
    console.log('product category butter get method requested');
    const Pcategory=request.params.Pcategory;
    const statement=`select * from Products where Pcategory= "ghee" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/icecream',(request,response)=>{
    console.log('product category butter get method requested');
    const Pcategory=request.params.Pcategory;
    const statement=`select * from Products where Pcategory= "ice-cream" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/paneer',(request,response)=>{
    console.log('product category butter get method requested');
    const Pcategory=request.params.Pcategory;
    const statement=`select * from Products where Pcategory= "paneer" `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/product',upload.single('Pthumbnail'),((request,response)=>{
    console.log('product post method requested');
    const {Pname,Prate,Pcategory,PQuantity } = request.body;

    const statement=`insert into Products (Pname,Prate,Pcategory,PQuantity,Pthumbnail) 
                    values('${Pname}','${Prate}','${Pcategory}','${PQuantity}','${request.file.filename}')`;
    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        notification.send(() => {
        response.send(utils.createResponse(error,result));
    });
    })
}));

router.put('/product/:Pid',(request,response)=>{
    console.log('product put method requested');
    const Pid=request.params.Pid;
    const { Pname,Prate,Pcategory,PQuantity,Pthumbnail }=request.body;
    const statement=`update Products set 
                    Pname='${Pname}',Prate='${Prate}',
                    Pcategory='${Pcategory}',PQuantity='${PQuantity}'
                    where Pid=${Pid} `;
    const connection=db.connect();
    //upload.single('Pthumbnail'),
    // , Pthumbnail='${request.file.filename}'
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/product/:id',(request,response)=>{
    console.log('product delete method requested..');

    const Pid=request.params.id;
    
    const statement=`delete from Products where Pid='${Pid}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

// Milk Rate Table Services
//========================================================================================
router.get('/milkrate',(request,response)=>{
    console.log('milk rate table get method requested .... ');
    const connection=db.connect();
    const statement=`select 
                    Fat , Buffalo,Cow ,Updated_on,Sr_No from Milk_rate`;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    }); 
});


router.post('/milkrate',(request,response)=>{
    console.log('milk rate tables post method requested..');
  
    const { Fat ,Buffalo ,Cow }=request.body;
    
    const connection=db.connect();
    
    const statement=`insert into Milk_rate 
                    (Fat , Buffalo, Cow ,Updated_on ) 
                    values
                    (${Fat},${Buffalo},${Cow},DATE(NOW())) `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


router.put('/milkrate/:Sr_No',(request,response)=>{
    console.log('milk rate tables put method requested..');
    const Sr_No = request.params.Sr_No;
    const { Fat , Buffalo, Cow }=request.body;
    
    const connection=db.connect();
    
    const statement=`update Milk_rate set
                        Fat='${Fat}',Buffalo='${Buffalo}',Cow='${Cow}' 
                    where 
                    Sr_No='${Sr_No}' `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/milkrate/:Sr_No',(request,response)=>{
    console.log('milk rate delete method requested..');
    const Sr_No=request.params.Sr_No;
    
    const connection=db.connect();
    
    const statement=`delete from Milk_rate where Sr_No= ${Sr_No} `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

//Customers related services
//=============================================================================================

router.get('/customer',(request,response)=>{
    console.log('customer get method requested');

    const statement=`select Id,Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail from Customer`;
    
    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/customer/:Id',(request,response)=>{
    console.log('customer get method requested');
    const Id=request.params.Id;
    const statement=`select Id,Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail from Customer where Id='${Id}' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result[0]));
    });
});

router.get('/customer/profile/:Id',(request,response)=>{
    console.log('customer get profile method requested');
    const Id=request.params.Id;
    const statement=`select * from Customer where Id='${Id}' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        if(result.length == 0){
            response.send(utils.createResponse('user not found', 'user not found'));
        }else {
            const customer = result[0];
            response.send(utils.createResponse(error,{
                Id: customer.Id,
                Cname: customer.Cemail,
                Phone: customer.Phone,
                Cthumbnail: customer.Cthumbnail
            }));
        }
    });
});

router.post('/customer',upload.single('Cthumbnail'),(request,response)=>{
    console.log('customer post method requested');
    const {Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail } = request.body;

    const statement=`insert into Customer 
                        (Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail) 
                    values
                    ('${Cname}','${Carea}','${Pin}','${Phone}',
                    '${Cemail}','${Cusername}','${Cpassword}','${Cgender}','${Cthumbnail}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/customer/:id',(request,response)=>{
    console.log('customer put method requested');
    const Id=request.params.id;
    const { Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail }=request.body;
    const statement=`update Customer set 
                        Cname='${Cname}', Carea='${Carea}',Pin='${Pin}',
                        Phone='${Phone}', Cemail='${Cemail}', 
                        Cusername='${Cusername}',Cpassword='${Cpassword}',
                        Cgender='${Cgender}',Cthumbnail='${Cthumbnail}' 
                    where 
                        Id='${Id}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/customer/:id',(request,response)=>{
    console.log('customer delete method requested..');

    const Id=request.params.id;
    
    const statement=`delete from Customer where Id='${Id}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/customer/signin',(request,response)=>{
    console.log('customer sign in requested..');

    const { Cusername , Cpassword }=request.body;

    const connection=db.connect();
    const statement=`select * from Customer where Cusername='${Cusername}' and Cpassword='${Cpassword}' `;
    
    connection.query(statement,(error,result)=>{
        connection.end();

        let status='';
        let data=null;

        if(error == null){
            if(result.length == 0){
                status = 'error';
                data = 'Invalid username or password';
            }
            else{
                status = 'success';
                data = result[0];
            }
        }else{
            status = 'error';
            data = error;
        }
        response.send({
            status : status,
            data : data
        });
    });
});


router.post('/customer/signup',(request,response)=>{
    console.log('customer requested for sign up ');

    const { Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender }=request.body;
    const connection=db.connect();
    const statement=`insert into Customer 
                        (Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender) 
                    values('${Cname}','${Carea}','${Pin}','${Phone}'
                    ,'${Cemail}','${Cusername}','${Cpassword}','${Cgender}') `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


//Dealers related services
//=============================================================================================

router.get('/dealer',(request,response)=>{
    console.log('dealer get method requested');

    const statement=`select Id,Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail,Dthumbnail from Dealers`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/dealer/:Id',(request,response)=>{
    console.log('dealer get details method requested');
    const Id=request.params.Id;
    const statement=`select Id,Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail,Dthumbnail from Dealers where Id='${Id}' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result[0]));
    });
});

router.post('/dealer',(request,response)=>{
    console.log('dealer post method requested');
    const { Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail,Dthumbnail } = request.body;

    const statement=`insert into Dealers 
                        ( Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail,Dthumbnail) 
                    values
                    ('${Dname}','${Daddress}','${Dphone}','${Agency_name}','${Dusername}',
                    '${Dpassword}','${Age}','${Gender}','${Demail}','${"dealer.jpg"}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/dealer/:id',(request,response)=>{
    console.log('dealer put method requested');
    const Id=request.params.id;
    const { Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail,Dthumbnail }=request.body;
    const statement=`update Dealers set 
                        Dname='${Dname}', Daddress='${Daddress}',
                        Dphone='${Dphone}',Agency_name='${Agency_name}',
                        Dusername='${Dusername}', Dpassword='${Dpassword}', 
                        Age='${Age}',Gender='${Gender}',Demail='${Demail}',Dthumbnail='${Dthumbnail}' 
                    where 
                        Id='${Id}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/dealer/:id',(request,response)=>{
    console.log('dealer delete method requested..');

    const Id=request.params.id;
    
    const statement=`delete from Dealers where Id='${Id}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

//Staff related services
//=============================================================================================

router.get('/staff',(request,response)=>{
    console.log('staff get method requested');

    const statement=`select Id,Sname,Saddress,Semail,DeptId,Srole,Ssalary,
                        Susername,Spassword,Sgender,Sage,Sphone,Sthumbnail from Staff`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/staff/:Id',(request,response)=>{
    console.log('staff get details method requested');
    const Id=request.params.Id;
    const statement=`select Id,Sname,Saddress,Semail,DeptId,Srole,Ssalary,
    Susername,Spassword,Sgender,Sage,Sphone,Sthumbnail from Staff where Id=${Id} `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result[0]));
    });
});

router.post('/staff',(request,response)=>{
    console.log('staff post method requested');
    const { Sname,Saddress,Semail,DeptId,Srole,Ssalary,
            Susername,Spassword,Sgender,Sage,Sphone,Sthumbnail } = request.body;

    const statement=`insert into Staff 
                        ( Sname,Saddress,Semail,DeptId,Srole,Ssalary,
                            Susername,Spassword,Sgender,Sage,Sphone,Sthumbnail) 
                    values
                    ('${Sname}','${Saddress}','${Semail}','${DeptId}','${Srole}',
                    '${Ssalary}','${Susername}','${Spassword}','${Sgender}','${Sage}','${Sphone}','${Sthumbnail}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.post('/staff/signin',(request,response)=>{
    console.log('Staff sign in requested..');

    const { Susername , Spassword }=request.body;

    const connection=db.connect();
    const statement=`select * from Staff where Susername='${Susername}' and Spassword='${Spassword}' and Srole="manager"`;
    
    connection.query(statement,(error,result)=>{
        connection.end();

        let status='';
        let data=null;

        if(error == null){
            if(result.length == 0){
                status = 'error';
                data = 'Invalid username or password';
            }
            else{
                status = 'success';
                data = result[0];
            }
        }else{
            status = 'error';
            data = error;
        }
        response.send({
            status : status,
            data : data
        });
    });
});


router.put('/staff/:id',(request,response)=>{
    console.log('staff put method requested');
    const Id=request.params.id;
    const { Sname,Saddress,Semail,DeptId,Srole,Ssalary,
            Susername,Spassword,Sgender,Sage,Sphone,Sthumbnail }=request.body;
    const statement=`update Staff set 
                        Sname='${Sname}', Saddress='${Saddress}',
                        Semail='${Semail}',DeptId='${DeptId}',
                        Srole='${Srole}', Ssalary='${Ssalary}', 
                        Susername='${Susername}',Spassword='${Spassword}',Sgender='${Sgender}',
                        Sage='${Sage}',Sphone='${Sphone}',Sthumbnail='${Sthumbnail}'
                    where 
                        Id='${Id}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/staff/:id',(request,response)=>{
    console.log('staff delete method requested..');

    const Id=request.params.id;
    
    const statement=`delete from Staff where Id='${Id}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


//Feedback page related services
//===========================================================================================

router.get('/feedback',(request,response)=>{
    console.log('feedback get method requested');

    const statement=`select cust_id,Name,Address,Taluka,District,Mobile,Email,fb,date,Response from Feedback`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/feedback/:Name',(request,response)=>{
    console.log('feedback details get method requested');
    const Name=request.params.Name;
    const statement=`select Name,Address,Taluka,District,Mobile,Email,fb,date,Response from Feedback where Name='${Name}'`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/feedback',(request,response)=>{
    console.log('feedback post method requested');
    const {Name,Address,Taluka,District,Mobile,Email,fb,date,Response } = request.body;

    const statement=`insert into Feedback 
                        (Name,Address,Taluka,District,Mobile,Email,fb,date,Response) 
                    values
                    ('${Name}','${Address}','${Taluka}','${District}',
                    '${Mobile}','${Email}','${fb}',Now(),'${Response}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/feedback/:Name',(request,response)=>{
    console.log('feedback put method requested');
    const Name=request.params.Name;
    const { Response }=request.body;
    const statement=`update Feedback set 
                        Response='${Response}'
                    where Name='${Name}' `;
    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/feedback/:Name',(request,response)=>{
    console.log('feedback delete method requested..');

    const Name=request.params.Name;
    
    const statement=`delete from Feedback where Name='${Name}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

//Registration table related information
//=============================================================================

router.get('/registration/',(request,response)=>{
    console.log('registration get method requested');
    const Name=request.params.Name;
    const statement=`select Id,Name,Phone,Username from Registration `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


//Orders table related information
//=============================================================================

router.post('/cart', (request, response) => {
    const {cust_id, Pid, Prate } = request.body;
    const queryStatement = `select * from Orders where cust_id = ${cust_id} and Pid = ${Pid}`;
    const connection = db.connect();
    connection.query(queryStatement, (error, result) => {
        // check if record exists for user and item pair
        if (result.length == 0) {

            // user is adding the item into the cart for  the first time
            const statement = `insert into Orders (cust_id, Pid, Prate) values (${cust_id}, ${Pid}, ${Prate})`;
            connection.query(statement, (error, result) => {
                connection.end();
                response.send(utils.createResponse(error, result));
            });
        } else {

            // user already has added the item to the cart earlier
            const Quantity = result[0]['Quantity'];
            const Oid = result[0]['Oid'];
            const statement = `update Orders set Quantity = ${Quantity + 1} where Oid = ${Oid}`;
            connection.query(statement, (error, result) => {
                connection.end();
                response.send(utils.createResponse(error, result));
            });
        }
    });    
})

router.delete('/cart/:Oid', (request, response) => {
    const Oid = request.params.Oid;
    const statement = `delete from Orders  where Oid = ${Oid}`;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    });
})

router.put('/cart/:Oid', (request, response) => {
    
    const Oid = request.params.Oid;
    const { Quantity } = request.body;

    const statement = `update Orders set Quantity = ${Quantity} where Oid = ${Oid}`;
    const connection = db.connect();

    console.log(statement);
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    });
})

router.get('/cart/:cust_id', (request, response) => {
    const cust_id = request.params.cust_id;
    const statement = `select Orders.Oid as orderId, Products.Pid as Pid, Orders.*, Products.* from Products, Orders Where cust_id = ${cust_id} and Products.Pid = Orders.Pid`;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    });
})

//by now purchase table 

// router.post('/orders', (request, response) => {
//     const {cust_id, Pid, Prate } = request.body;
//     const queryStatement = `select SUM(Prate) from Orders where cust_id = ${cust_id}`;
//     const connection = db.connect();
//     connection.query(queryStatement, (error, result) => {
//         // check if record exists for user and item pair
//         if (result.length == 0) {

//             // user is adding the item into the cart for  the first time
//             const statement = `insert into Purchase (cust_id,SUM(Prate) values (${cust_id}, ${Prate}) where cust_id='${cust_id}`;
//             connection.query(statement, (error, result) => {
//                 connection.end();
//                 response.send(utils.createResponse(error, result));
//             });
//         } else {
//             console.log('thank you');
//             // user already has added the item to the cart earlier
//             // const Quantity = result[0]['Quantity'];
//             // const Oid = result[0]['Oid'];
//             // const statement = `update Orders set Quantity = ${Quantity + 1} where Oid = ${Oid}`;
//             // connection.query(statement, (error, result) => {
//             //     connection.end();
//             //     response.send(utils.createResponse(error, result));
//             //});
//         }
//     });    
// })

module.exports=router;

